# ChangeLog

## 0.3.1 (2020-07-15)

- filter exceptions caused by the bpo-33263 workaround


## 0.3.0 (2019-11-26)

- removed the monkey patch for asyncio streams (the package now works with the
  stdlib unmodified)


## 0.2.1 (2019-11-07)

- first public release
